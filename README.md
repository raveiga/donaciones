# Donaciones de Sangre
![Imagen](https://w0.pngwave.com/png/984/934/public-relations-public-transport-communication-blood-donation-png-clip-art.png)

# Debido a la crisis del **COVID-19**, el sistema de donaciones de sangre ha cambiado y para ello se solicita crear una aplicación web que permita llevar el control de turnos en las donaciones de sangre.

De los/as **donantes de sangre** se necesita almacenar los siguientes datos (**todos los campos son obligatorios**):

- Nombre
- Apellidos
- Edad (mayor de 18 años)
- Grupo Sanguíneo (O-,O+,A-,A+,B-,B+,AB-,AB+)
- Localidad
- Código postal  (5 dígitos)
- Teléfono móvil (9 dígitos)

Necesitamos llevar un **histórico de todas las donaciones** que realizó cada donante. Para ello **en el momento de finalizar la donación** se almacenarán los siguientes datos:
- donante
- fecha donación
- fecha próxima donación (4 meses posteriores a la fecha de la donación - se cubrirá automáticamente).

**La aplicación deberá permitir**:

- **Altas y Listado de Donantes** (en el listado de donantes aparecerá para cada registro: **Editar | Borrar | Listar mis donaciones**)
- El **listado de donaciones** del donante deberá mostrar en la parte superior:
   - Título del listado con: **Nombre y apellidos, Edad, Grupo Sanguíneo**
   - Y por debajo del título una **tabla** con todos los datos de sus donaciones ordenados por **fecha de donación descendente**.
- **Búsqueda de donantes por código postal** y opcionalmente **grupo sanguíneo**:
   - Se introducirá un código postal y se seleccionará opcionalmente un grupo sanguíneo en un desplegable y se mostrarán todos los/as donantes que pertenezcan a ese código postal y opcionalmente a ese grupo sanguíneo, y que además la fecha de su próxima donación sea posterior a la fecha actual o que no tengan todavía ninguna donación registrada.
   - Se mostrará en el listado el **Nombre, Apellidos, Teléfono móvil, Grupo Sanguíneo, Fecha donación y Fecha Próxima donación**.
- Cuando se **borre un donante** se borrarán automáticamente todas sus donaciones.
- Los datos deberán ser validados evitando XSS e inyección MySQL. Tenéis una librería de funciones útiles en la **carpeta inc**.
- El enlace **registrar donación** mostrará un desplegable con nombre apellidos y teléfono móvil (ordenado por los dos primeros campos) y al darle al botón **registrar donación** registrará en la tabla de donaciones la fecha de la donación y la fecha próxima. Se mostrará como resultado un texto con la fecha de la próxima de donación (4 meses más de la fecha actual).
- **ATENCIÓN: antes de registrar la donación** hay que tener en cuenta si se puede o no realizar la donación (que hayan pasado 4 meses desde la última donación o que no haya hecho ninguna donación todavía).
- Por último tendremos una página independiente no referenciada con ningún enlace, llamada **altaadmin.php** que se encarga de dar de alta los/as administradores/as de la web con los siguientes campos:
   - **nick** (clave primaria) varchar 50
   - **contraseña** (tendrá que estar encriptada) varchar 200
- Esta tabla de administradores/as nos permitirá llevar el control de los/as administradores/as de la web (podrán acceder a todas las opciones del menú izquierdo).
   - Los/as administradores/as cuando hagan **Login** en la web, se les mostrará el menú izquierdo con todas las opciones disponibles.
   - Cuando estén logueados/as les aparecerá la opción de **Desconectar** en el menú superior derecha para desconectarse de la web.
    - Si no se está logueado, en el menú izquierdo podéis escribir un texto con información de la aplicación.
- **Dar seguridad a todas las páginas privadas del menú** para que solamente permita el acceso a administradores/as.

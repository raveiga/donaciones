<?php

class Basedatos
{
    // Propiedad que almacen la conexión a la base de datos
    // Será un objeto de la clase PDO
    private static $conexion = false;
    private $config;

    // Creamos la conexión a la base de datos.
    private function __construct()
    {
        $this->config = parse_ini_file('config.ini', true);

        try {
            //print_r($this->config);
            $stringConexion = "mysql:host=" . $this->config['host'] . ";dbname=" . $this->config['basedatos'] . ";charset=utf8";
            self::$conexion = new PDO($stringConexion, $this->config['usuario'], $this->config['pass']);
            self::$conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $error) {
            die("Error conectando al servidor de MySQL:" . $error->getMessage());
        }
    }

    // En el patron Singleton se usa para asegurar que hay solamente
    // una instancia de una clase, y se puede acceder a ella globalmente.
    // Creamos un método público que nos dará acceso a esa instancia.
    public static function getConexion()
    {
        // Comprobamos si hay una conexion hecha.
        if (!self::$conexion) {
            // Creamos la conexion
            // Una opción sería:
            // self::__construct();

            // Otra opción
            new self;
        }

        // Devolver el objeto PDO.
        return self::$conexion;
    }
}

<?php

// Funciones interesantes.

function filtrar_input($dato)
{
    $dato = trim($dato); // Eliminamos espacios en blanco al principio y al final
    $dato = stripslashes($dato); // Elimina las barras invertidas \ . Ejemplo Bienvenido O\'Reilly quedaría como Bienvenido O'Reilly
    $dato = htmlspecialchars($dato); // Convierte caracteres especiales como < > en entidades HTML &lt; &gt; Para evitar XSS (Cross Site Scripting)

    return $dato;
}

function filtrar_campos()
{
    // Si estamos recibiendo datos por POST
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Recorremos todos los campos y los filtramos.
        foreach ($_POST as $campo => $valor) {
            $_POST[$campo] = filtrar_input($valor);
        }
    }
    // O si recibimos por GET
    else if ($_SERVER["REQUEST_METHOD"] == "GET") {
        // Recorremos todos los campos y los filtramos.
        foreach ($_GET as $campo => $valor) {
            $_GET[$campo] = filtrar_input($valor);
        }
    }
}

function validar_nif($dni)
{
    $lista = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"];
    $letra = substr($dni, strlen($dni) - 1, 1);
    $numero = intval(substr($dni, 0, strlen($dni) - 1)) % 23;
    if (strtoupper($letra) == $lista[$numero]) {
        return true;
    } else {
        return false;
    }
}

/**
* Función que encripta una password utilizando SHA-512 y 10000 vueltas por defecto
*
* @param string $password
* @param int $vueltas Número de vueltas, opcional. Por defecto 10000.
* @return string
*/
function encriptar($password, $vueltas = 10000) {
// Empleamos SHA-512 con 10000 vueltas por defecto.

    $salt = substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);

    return crypt((string) $password, '$6$' . "rounds=$vueltas\$$salt\$");

// Forma de comprobación de contraseña en login:
// Es recomendable comparar las cadenas de hash con hash_equals que es una comparación segura contra ataques de timing.

// Ejemplo de uso (hash_equals para evitar ataques de Timing)

// if (hash_equals(crypt($passRecibidaFormulario, $passwordAlmacenadaEncriptada),$passwordAlmacenadaEncriptada))

// Es necesario pasarle a la función crypt la $passwordAlmacenadaEncriptada para que pueda extraer la semilla, el tipo de encriptación y el número de vueltas.
}


function fecha_a_mysql($fecha)
{
    if ($fecha==NULL)
        return '';

    $fecha=str_replace('-','/',$fecha);

    if (strlen($fecha)>10)
        return DateTime::createFromFormat("d/m/Y H:i:s", $fecha)->format('Y-m-d H:i:s');
    else
        return DateTime::createFromFormat("d/m/Y", $fecha)->format('Y-m-d');
    //return $convertida->format('Y-m-d H:i:s');
}

function mysql_a_fecha($fecha)
{
    if ($fecha==NULL)
        return '';
    if (strlen($fecha)>10)
        return DateTime::createFromFormat("Y-m-d H:i:s", $fecha)->format('d/m/Y H:i:s');
    else
        return DateTime::createFromFormat("Y-m-d", $fecha)->format('d/m/Y');
}


function mysql_a_fecha_corta($fecha)
{
    if ($fecha==NULL)
        return '';
    if (strlen($fecha)>10)
        return DateTime::createFromFormat("Y-m-d H:i:s", $fecha)->format('d/m/Y');
    else
        return DateTime::createFromFormat("Y-m-d", $fecha)->format('d/m/Y');
}


function mysql_a_fecha_corta_avisos($fecha)
{
    if ($fecha==NULL)
        return '';
    if (strlen($fecha)>10)
        return DateTime::createFromFormat("Y-m-d H:i:s", $fecha)->format('d/m/Y \a \l\a\s H:i');
    else
        return DateTime::createFromFormat("Y-m-d", $fecha)->format('d/m/Y');
}


function mysql_a_hora_corta($fecha)
{
    if ($fecha==NULL)
        return '';
    if (strlen($fecha)>10)
        return DateTime::createFromFormat("Y-m-d H:i:s", $fecha)->format('H:i');
    else
        return DateTime::createFromFormat("Y-m-d", $fecha)->format('H:i');
}




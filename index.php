<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Gestión de donaciones de sangre">
    <meta name="author" content="Rafa Veiga">
    <title>Gestión Donaciones de Sangre</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style>
    .bd-placeholder-img {
    font-size: 1.125rem;
    text-anchor: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    }

    @media (min-width: 768px) {
    .bd-placeholder-img-lg {
    font-size: 3.5rem;
    }
    }
    </style>
</head>

<body>
    <nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
        <a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="index.php">Gestión Donaciones de Sangre</a>
        <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-toggle="collapse" data-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="login.php"><img src="img/icons/door-open.svg" alt="" width="18" height="18" title="Buscar"> Login</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <!-- Menú web -->
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="sidebar-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" href="index.php"><img src="img/icons/house-door.svg" alt="" width="18" height="18" title="House"> Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="alta.php"><img src="img/icons/person-plus.svg" alt="" width="18" height="18" title="Alta"> Alta Donantes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="listado.php"><img src="img/icons/person-lines-fill.svg" alt="" width="18" height="18" title="Listado"> Listado Donantes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="buscar.php"><img src="img/icons/search.svg" alt="" width="18" height="18" title="Buscar"> Buscar Donantes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="donar.php"><img src="img/icons/journal-check.svg" alt="" width="18" height="18" title="Registrar"> Registrar Donación</a>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- Fin menú web -->

            <!-- Parte central de la web -->
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">

                <div class="d-flex justify-content-center">
                    <img src="img/cartel.jpg" alt="Portada donaciones de sangre">
                </div>

            </main>
            <!-- Fin parte central de la web -->
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="/docs/4.5/assets/js/vendor/jquery.slim.min.js"><\/script>')
    </script>
</body>

</html>